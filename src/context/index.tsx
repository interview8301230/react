import React from "react";

import { Props } from "../types";

const AppContext = React.createContext<any>(null);

export const AppWrapper: React.FC<Props> = ({ children }) => {
  return <AppContext.Provider value={{}}>{children}</AppContext.Provider>;
};

export const UseAppContext = () => {
  return React.useContext(AppContext);
};
