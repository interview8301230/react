import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./components/HomePage";
import Todo from "./components/TodoPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/:id" element={<Todo />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
