import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

const validationSchema = Yup.object({
  title: Yup.string().trim().required(),
  description: Yup.string().trim().required(),
  status: Yup.string().required(),
});

function TodoForm() {
  const initialValues = { title: "", description: "", status: "TODO" };

  const submitHandler = () => {};

  return (
    <section>
      <h2>Add a new task</h2>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={submitHandler}
      >
        <Form>
          <div>
            <label htmlFor="title">Title</label>
            <Field id="title" name="title" />
          </div>
          <div>
            <label htmlFor="description">Description</label>
            <Field id="description" name="description" />
          </div>
          <button type="submit">Submit</button>
        </Form>
      </Formik>
    </section>
  );
}

export default TodoForm;
