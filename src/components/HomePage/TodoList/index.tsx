import { Link } from "react-router-dom";

function TodoItem(props: any) {
  return (
    <li key={props.id}>
      <Link to={`/${props.id}`}>
        <h3>{props.title}</h3>
        <p>{props.description}</p>
        <p>{props.status}</p>
      </Link>
    </li>
  );
}

function TodoList() {
  return (
    <section>
      <h2>Tasks</h2>
      <ul>{/* todoZ */}</ul>
    </section>
  );
}

export default TodoList;
