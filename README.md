1- Add A Create Logic For Todos Initial Value for The Status Of Each Todo Should Be 'TODO'
2- You Can Go To Next Todo Or Previous Todo If Available
3- You Can Update The Status Of The Todo In This Way:
    1: When The Status Is "TODO" It Only Could Be Update To "IN_PROGRESS"
    2: When The Status Is "IN_PROGRESS" It Could Be Update To "BLOCKED" Or "IN_QA"
    3: When The Status Is "BLOCKED" It Only Could Be Update To "TODO"
    4: When The Status Is "IN_QA" It Could Be Update To "DONE" Or "TODO"
    5: When The Status Is "DONE" It Only Could Be Update To "DEPLOYED"
    6: When The Status Is "DEPLOYED" It Can Not Be Updated Any More